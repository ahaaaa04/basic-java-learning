package ahaaaa.cn.net.tcp.tcp3;

import java.io.DataInputStream;
import java.io.InputStream;
import java.net.Socket;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 11:31
 * @Description
 */
public class ClientReaderThread extends Thread{

    private Socket socket;

    public ClientReaderThread(Socket socket){
        this.socket = socket;
    }

    @Override
    public void run() {

        try {

            // 从管道中获取输入流
            InputStream inputStream = socket.getInputStream();
            DataInputStream dataInputStream = new DataInputStream(inputStream);

            // 处理
            while (true){
                try {
                    String readUTF = dataInputStream.readUTF();
                    System.out.println(readUTF);
                }catch (Exception e){
                    System.out.println(socket.getLocalSocketAddress() + "自己离线了！");
                    dataInputStream.close();
                    socket.close();
                    break;
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
