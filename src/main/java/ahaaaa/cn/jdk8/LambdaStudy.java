package ahaaaa.cn.jdk8;

import org.junit.Test;
import java.util.*;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 17:40
 * @Description
 */
public class LambdaStudy {

    @Test
    public void test1(){
        Map<String, Integer> map = new HashMap<>();
        map.put("ahaaa",11);
        map.put("cpx",23);
        map.put("lch",4245);
        map.put("chamu",35);

        Set<Map.Entry<String, Integer>> entries = map.entrySet();

        // 原本 （这里的stream()等都是Stream流的应用，大家可以先忽略，关注排序sorted()方法里面的变化即可
        entries.stream().sorted(new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        }).forEach(e ->  System.out.println(e.getKey()+"----"+e.getValue()));

        // lambda改写后（省略了o1，o2的参数类型、省略了{ }、省略了return和 ; 号）
        entries.stream().sorted((o1, o2) -> o1.getValue() - o2.getValue())
                .forEach(e ->  System.out.println(e.getKey()+"----"+e.getValue()));
    }

    @Test
    public void test2(){

    }

    @Test
    public void test3(){

    }

    @Test
    public void test4(){

    }

}
