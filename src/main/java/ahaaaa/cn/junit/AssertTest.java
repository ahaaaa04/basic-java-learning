package ahaaaa.cn.junit;

import org.junit.Assert;
import org.junit.Test;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 14:56
 * @Description
 */
public class AssertTest {

    @Test
    public void test1(){
        AssertStudy assertStudy = new AssertStudy();
        int age = assertStudy.getAge();
        Assert.assertEquals(19, age);
    }

}
