package ahaaaa.cn.net.tcp.tcp3;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-03 18:17
 * @Description tcp多发客户端
 */
public class TCPCommunicationClient {

    public static void main(String[] args) throws Exception {

        // 客户端对象
        Socket socket = new Socket("127.0.0.1", 9999);

        new ClientReaderThread(socket).start();

        // 从通信管道获取输出流
        OutputStream outputStream = socket.getOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

        Scanner scanner = new Scanner(System.in);

        // 处理数据
        while (true){

            System.out.print("please say:");
            String message = scanner.nextLine();

            if ("exit".equals(message)){
                System.out.println("exit success!");
                dataOutputStream.close();
                socket.close();
                break;
            }

            dataOutputStream.writeUTF(message);
            dataOutputStream.flush();

        }

    }
}
