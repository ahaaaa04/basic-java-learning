package ahaaaa.cn.net.tcp.tcp2;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-03 18:18
 * @Description 服务端接收多个客户端
 */
public class TCPCommunicationServer {

    public static void main(String[] args) throws Exception {

        System.out.println("-----TCPCommunicationServer Start-----");

        // 服务端
        ServerSocket serverSocket = new ServerSocket(9999);

        // 处理
        while (true){
            // 等待请求
            Socket accept = serverSocket.accept();

            System.out.println("有人上线了：" + accept.getRemoteSocketAddress());

            new ServerReaderThread(accept).start();

        }

    }

}
