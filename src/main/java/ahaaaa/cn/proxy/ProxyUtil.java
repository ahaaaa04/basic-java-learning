package ahaaaa.cn.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 16:49
 * @Description
 */
public class ProxyUtil {

    public static Person createProxy(Boss boss){

        Person person = (Person) Proxy.newProxyInstance(ProxyUtil.class.getClassLoader(), new Class[]{Person.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.getName().equals("speak")){
                    System.out.println("代理助手：大家来一下，林老板有话要讲。");
                }else if (method.getName().equals("praise")){
                    System.out.println("代理助手：大家安静下，"+boss.getName()+"还有一件事要宣布。");
                }
                return method.invoke(boss);
            }
        });
        return person;
    }


}
