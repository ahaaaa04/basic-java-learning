package ahaaaa.cn.jdk8;

import org.junit.Test;

import java.util.*;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 17:55
 * @Description
 */
public class MethodReferences {

    @Test
    public void test1(){
        List<Cat> objects = new ArrayList<>();
        Cat hei = new Cat("小黑", 11);
        Cat bai = new Cat("小白", 23);
        Cat lan = new Cat("小蓝", 45);
        Cat hong = new Cat("小红", 64);

        objects.add(hei);
        objects.add(bai);
        objects.add(lan);
        objects.add(hong);

        // 原本
//        objects.stream().sorted(new Comparator<Cat>() {
//            @Override
//            public int compare(Cat o1, Cat o2) {
//                return o1.getAge()-o2.getAge();
//            }
//        }).forEach(e -> System.out.println(e.toString()));

        // lambda
//        objects.stream().sorted((o1, o2) -> o1.getAge()-o2.getAge())
//                .forEach(e -> System.out.println(e.toString()));

        // 调用方法
//        objects.stream().sorted((o1, o2) -> CompareByAge.CopareByAgeAsc(o1, o2))
//                .forEach(e -> System.out.println(e.toString()));

        // 静态方法引用
//        objects.stream().sorted(CompareByAge::CopareByAgeAsc)
//                .forEach(e -> System.out.println(e.toString()));

        // 实例方法引用
        CompareByAge compareByAge = new CompareByAge();

//        objects.stream().sorted((o1, o2) -> compareByAge.CopareByAgeDesc(o1, o2))
//                .forEach(e -> System.out.println(e.toString()));

        objects.stream().sorted(compareByAge::CopareByAgeDesc)
                .forEach(e -> System.out.println(e.toString()));

    }

    @Test
    public void test2(){

        String[] strings = new String[]{"banana","ahaaaa","fix","available"};

        // origin
//        Arrays.sort(strings, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.compareTo(o2);
//            }
//        });

        // lambda
//        Arrays.sort(strings, (o1, o2) -> o1.compareTo(o2));

        // 特定类型方法引用
        Arrays.sort(strings, String::compareTo);

        for (String string : strings) {
            System.out.println(string);
        }

    }

    @Test
    public void test3(){

        // 原本
        Kitty kitty = new Kitty() {
            @Override
            public Cat eat(String name, int age) {
                return new Cat(name, age);
            }
        };

        // lambda
//        Kitty kitty =(name, age) -> new Cat(name, age);

        // 构造器引用
//        Kitty kitty =Cat::new;
        System.out.println(kitty.eat("鱼儿", 8));


    }

    interface Kitty{
        Cat eat(String name, int age);
    }

}
