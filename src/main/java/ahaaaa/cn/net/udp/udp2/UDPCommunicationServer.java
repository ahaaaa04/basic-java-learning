package ahaaaa.cn.net.udp.udp2;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-03 17:40
 * @Description 服务端接收信息
 */
public class UDPCommunicationServer {
    public static void main(String[] args) throws Exception {

        System.out.println("-----UDPCommunicationServer Start-----");
        // 服务端
        DatagramSocket datagramSocket = new DatagramSocket(9999);

        // 数据对象接收数据
        byte[] bytes = new byte[64 * 1024];
        DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length);

        while(true){
            datagramSocket.receive(datagramPacket);
            // 处理数据
            int length = datagramPacket.getLength();
            String message = new String(bytes, 0, length);
            System.out.println(message);
            // 获取对方主机名和地址
            System.out.println(datagramPacket.getAddress().getHostName());
            System.out.println(datagramPacket.getAddress().getHostAddress()+":"+datagramPacket.getPort());
            System.out.println("---------------------------------------------------");
        }


    }
}
