package ahaaaa.cn.net.tcp.tcp1;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-03 18:18
 * @Description 服务端多收
 */
public class TCPCommunicationServer {

    public static void main(String[] args) throws Exception {

        System.out.println("-----TCPCommunicationServer Start-----");

        // 服务端
        ServerSocket serverSocket = new ServerSocket(9999);

        // 等待请求
        Socket accept = serverSocket.accept();

        // 从管道中获取输入流
        InputStream inputStream = accept.getInputStream();
        DataInputStream dataInputStream = new DataInputStream(inputStream);

        // 处理
        while (true){
            try {
                String readUTF = dataInputStream.readUTF();
                System.out.println(readUTF);
            }catch (Exception e){
                System.out.println(accept.getRemoteSocketAddress() + "离线了！");
                dataInputStream.close();
                accept.close();
                break;
            }

        }




    }

}
