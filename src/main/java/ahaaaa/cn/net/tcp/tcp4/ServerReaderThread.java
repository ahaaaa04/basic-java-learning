package ahaaaa.cn.net.tcp.tcp4;

import java.io.*;
import java.net.Socket;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 11:31
 * @Description
 */
public class ServerReaderThread extends Thread{

    private Socket socket;

    public ServerReaderThread(Socket socket){
        this.socket = socket;
    }

    @Override
    public void run() {

        try {

            // 写入数据到管道中
            OutputStream outputStream = socket.getOutputStream();
            PrintStream printStream = new PrintStream(outputStream);

            printStream.println("HTTP/1.1 200 OK");
            printStream.println("Content-Type:text/html;charset=UTF-8");
            printStream.println();
            printStream.println("<div>hello ahaaaa!</div>");

            printStream.close();
            socket.close();

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
