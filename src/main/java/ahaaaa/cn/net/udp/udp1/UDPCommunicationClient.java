package ahaaaa.cn.net.udp.udp1;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-03 17:39
 * @Description 一发一收
 */
public class UDPCommunicationClient {

    public static void main(String[] args) throws Exception {
        // 客户端
        DatagramSocket datagramSocket = new DatagramSocket(8888);

        // 数据（最大不超过64kb）
        byte[] bytes = "The pursuit of youth and the pursuit of success !".getBytes(StandardCharsets.UTF_8);
        InetAddress localHost = InetAddress.getLocalHost();
        DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length, localHost, 9999);

        // 发收
        datagramSocket.send(datagramPacket);

        // 释放
        datagramSocket.close();
    }

}
