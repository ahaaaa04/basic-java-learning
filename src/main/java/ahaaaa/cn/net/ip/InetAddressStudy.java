package ahaaaa.cn.net.ip;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-03 17:31
 */
public class InetAddressStudy {

    public static void main(String[] args) throws UnknownHostException {
        // 获取ip对象
        InetAddress localHost = InetAddress.getLocalHost();

        String hostName = localHost.getHostName();
        String hostAddress = localHost.getHostAddress();
        byte[] address = localHost.getAddress();

        System.out.println("hostName:"+hostName+"   "+"hostAddress:"+hostAddress+"   "+"address:"+address);

        // 根据域名或ip获取ip地址对象
        InetAddress ip = InetAddress.getByName("www.baidu.com");
        byte[] address1 = ip.getAddress();
        String hostName1 = ip.getHostName();
        String hostAddress1 = ip.getHostAddress();
        System.out.println("address1:"+address1+"   "+"hostName1:"+hostName1+"  "+"hostAddress1:"+hostAddress1);

    }

}
