package ahaaaa.cn.annotation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 16:23
 * @Description
 */
@MyAnnotation(name = "ahaaaa", age = 21)
public class MyAnntationTest {

    @MyAnnotation(name = "chamu", age = 21)
    public void test1(){
        System.out.println("test1");
    }

    public void test2(){
        System.out.println("test2");
    }

    @MyAnnotation(name = "cpx", age = 20)
    public void test3(){
        System.out.println("test3");
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {

        // 获取注解类Class对象
        Class<MyAnntationTest> myAnntationTestClass = MyAnntationTest.class;
        Method[] declaredMethods = myAnntationTestClass.getDeclaredMethods();

        MyAnntationTest myAnntationTest = new MyAnntationTest();

        for (Method declaredMethod : declaredMethods) {
            // 判断方法MyAnnotation中注解是否存在
            if (declaredMethod.isAnnotationPresent(MyAnnotation.class)){
                // 获取方法上的注解对象
                MyAnnotation annotation = declaredMethod.getAnnotation(MyAnnotation.class);
                System.out.println(annotation.name());
                System.out.println(annotation.age());
                declaredMethod.invoke(myAnntationTest);
            }

        }


    }

}
