package ahaaaa.cn.net.tcp.tcp5;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-03 18:18
 * @Description 服务端多收
 */
public class TCPCommunicationServer {


    public static void main(String[] args) throws Exception {

        System.out.println("-----TCPCommunicationServer Start-----");

        // 服务端
        ServerSocket serverSocket = new ServerSocket(8080);

        // 创建线程池
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(8 * 2, 8 * 2, 0, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(8), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

        // 处理
        while (true){
            // 等待请求
            Socket accept = serverSocket.accept();
            System.out.println("有人上线了：" + accept.getRemoteSocketAddress());

            threadPoolExecutor.execute(new ServerReaderThread(accept));

        }




    }

}
