package ahaaaa.cn.proxy;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 16:46
 * @Description
 */
public interface Person {

    void speak();

    void praise();

}
