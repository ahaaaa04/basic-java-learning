package ahaaaa.cn.net.udp.udp2;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-03 17:39
 * @Description 客户端发送信息
 */
public class UDPCommunicationClient {

    public static void main(String[] args) throws Exception {
        // 客户端
        DatagramSocket datagramSocket = new DatagramSocket(8888);

        // 数据（最大不超过64kb）
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("please:");
            String message = scanner.nextLine();

            // 断开
            if ("exit".equals(message)){
                System.out.println("exit success!");
                datagramSocket.close();
                break;
            }

            byte[] bytes = message.getBytes(StandardCharsets.UTF_8);
            // 封装UDP传输中包所需要的的信息
            InetAddress localHost = InetAddress.getLocalHost();
            DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length, localHost, 9999);

            // 发送数据
            datagramSocket.send(datagramPacket);

        }
    }

}
