package ahaaaa.cn.reflect;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 15:02
 * @Description
 */
public class ObjectFrame {

    public static void saveObject(Object obj) throws Exception{

        PrintStream printStream = new PrintStream(new FileOutputStream("E:\\桌面\\basic-java-learning\\src\\main\\java\\ahaaaa\\cn\\reflect\\data.txt", true));

        Class<?> aClass = obj.getClass();

        String simpleName = aClass.getSimpleName();

        printStream.println("--------------"+simpleName+"--------------");
        for (Field declaredField : aClass.getDeclaredFields()) {
            declaredField.setAccessible(true);
            printStream.println(declaredField.get(obj));
        }

    }

}
