package ahaaaa.cn.proxy;

import org.junit.Test;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 16:56
 * @Description
 */
public class ProxyTest {

    @Test
    public void test1(){

        Boss boss = new Boss("林总");

        Person proxy = ProxyUtil.createProxy(boss);

        proxy.speak();
        proxy.praise();

    }

}
