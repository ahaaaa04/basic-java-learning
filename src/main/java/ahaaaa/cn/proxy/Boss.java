package ahaaaa.cn.proxy;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 16:44
 * @Description
 */
public class Boss implements Person{

    private String name;

    public Boss(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void speak() {
        System.out.println(name+"：公司上市了，大家辛苦了！");
    }

    @Override
    public void praise() {
        System.out.println(name+"：今年大家奖金翻倍！！！");
    }
}
