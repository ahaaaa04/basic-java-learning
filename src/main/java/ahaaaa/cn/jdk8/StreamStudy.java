package ahaaaa.cn.jdk8;

import org.junit.Test;

import java.util.*;
import java.util.stream.Stream;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 17:23
 * @Description
 */
public class StreamStudy {

    @Test
    public void test1(){
        // list转stream流
        List<String> list = new ArrayList<>();

        list.add("ahaaaa");
        list.add("banana");
        list.add("apple");
        list.add("grapes");
        list.add("watermelon");

        // 调用filter()进行过滤，判断集合中的对象e是否包涵“b”，若不包含过滤，然后调用forEach()进行遍历
        list.stream().filter(e -> e.contains("b")).forEach(e -> System.out.println(e));
        // 调用count()获取集合格个数
        System.out.println(list.stream().count());
    }


    @Test
    public void test2(){
        // map转stream流
        Map<String, Integer> map = new HashMap<>();
        map.put("ahaaa",11);
        map.put("cpx",23);
        map.put("lch",4245);
        map.put("chamu",35);
        map.put("zhuzhu",323);

        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        entries.stream().filter(e -> e.getKey().contains("a")).filter(e -> e.getValue() > 20)
                .forEach(e -> System.out.println(e.getKey()+"----"+e.getValue()));
    }

    @Test
    public void test3(){
        // 数组转stream流
        String[] names = {"ahaaaa","cpx","lch","chamu"};

        // 获取流方式1
//        Stream<String> namesStream = Stream.of(names);

        // 获取流方式2
        Stream<String> stream = Arrays.stream(names);
        // 调用filter()进行过滤，判断集合中的对象e是否包涵“p”，若不包含过滤，然后调用forEach()进行遍历
        stream.filter(e -> e.contains("p")).forEach(e -> System.out.println(e));

    }

    @Test
    public void test4(){


    }


}
