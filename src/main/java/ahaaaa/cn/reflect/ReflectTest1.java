package ahaaaa.cn.reflect;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 15:05
 * @Description
 */
public class ReflectTest1 {

    @Test
    public void classReflectTest() throws ClassNotFoundException {

        // 获取class对象-方法1
        Class<Cat> catClass1 = Cat.class;
        System.out.println(catClass1.getName());

        // 获取class对象-方法2
        Class<?> catClass2 = Class.forName("ahaaaa.cn.reflect.Cat");

        // 获取class对象-方法3
        Cat cat = new Cat();
        Class<? extends Cat> catClass3 = cat.getClass();
    }

    @Test
    public void constructorReflectTest(){

        // 获取class对象
        Class<Cat> catClass = Cat.class;

        // 获取Cat类的构造器
        Constructor<?>[] constructors = catClass.getDeclaredConstructors();

        for (Constructor<?> constructor : constructors) {
            System.out.println(constructor.getName()+"--->"+constructor.getParameterCount());
        }

    }

    @Test
    public void fieldReflectTest(){

        // 获取class对象
        Cat cat = new Cat();
        Class<? extends Cat> catClass = cat.getClass();

        // 获取Cat的成员变量
        Field[] fields = catClass.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getName());
        }
    }

    @Test
    public void methodReflectTest() throws Exception {
        // 获取class对象
        Class<?> catClass = Class.forName("ahaaaa.cn.reflect.Cat");
        Cat cat = new Cat();

        // 获取Cat的方法
        Method[] declaredMethods = catClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println(declaredMethod.getName()+"--->"+declaredMethod.getParameterCount());
        }

        // 获取方法后可以通过method.invoke（）来调用方法执行。第一个参数需要该方法所在类的一个对象，第二个参数为方法的参数（可以多个，没有可以不填）
        Method eat = catClass.getDeclaredMethod("eat",String.class);
        eat.setAccessible(true);
        eat.invoke(cat,"鱼儿");

    }


    @Test
    public void FrameReflectTest() throws Exception {

        Cat cat = new Cat("小黑", 11);

        Person person = new Person("ahaaaa", 178.5, 130);

        ObjectFrame.saveObject(cat);
        ObjectFrame.saveObject(person);

    }


}
