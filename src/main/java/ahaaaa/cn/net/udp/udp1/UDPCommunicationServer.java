package ahaaaa.cn.net.udp.udp1;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-03 17:40
 * @Description 一发一收
 */
public class UDPCommunicationServer {
    public static void main(String[] args) throws Exception {

        System.out.println("-----UDPCommunicationServer Start-----");
        // 服务端
        DatagramSocket datagramSocket = new DatagramSocket(9999);

        // 数据对象接收数据
        byte[] bytes = new byte[64 * 1024];
        DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length);
        datagramSocket.receive(datagramPacket);

        // 处理数据
        int length = datagramPacket.getLength();
        String message = new String(bytes, 0, length);
        System.out.println(message);
        System.out.println(datagramPacket.getAddress().getHostName());
        System.out.println(datagramPacket.getSocketAddress());

        // 释放
        datagramSocket.close();

    }
}
