package ahaaaa.cn.jdk8;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 14:59
 * @Description
 */
public class Cat {

    private String name;

    private int age;

    public Cat() {
    }

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }

    private void eat(String name){
        System.out.println("cat eat "+name+" !");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
