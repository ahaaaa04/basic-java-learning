package ahaaaa.cn.reflect;

/**
 * Description of the class.
 *
 * @Author ahaaaa
 * @Date 2023-09-04 15:00
 * @Description
 */
public class Person {

    private String name;

    private double height;

    private int weight;

    public Person() {
    }

    public Person(String name, double height, int weight) {
        this.name = name;
        this.height = height;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                '}';
    }
}
